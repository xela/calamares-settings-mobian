# Calamares Settings for the Mobian Installer
This repo serves as the location for our [Calamares](https://github.com/calamares/calamares) [extensions](https://github.com/calamares/calamares-extensions) configuration.

 - We use calamares as it has the benefit of being the same base installer for ourselves and pmOS (and perhaps others).
 - All of the Mobian installer UI and options come from the calamares-extensions package (lightly patched upstream code).

This repo is the configuration to make calamares use the right modules and install the system in the way Mobian users expect. 
Modifications to install scripts and config should be done here, work on the broader installer is best placed in upstream calamares-extensions.
